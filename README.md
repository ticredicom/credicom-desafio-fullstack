# Desafio Desenvolvedor FullStack Sicoob Credicom #

Primeiramente, obrigado pelo seu interesse ao se candidatar nessa oportunidade. Abaixo você encontrará todas as informações necessárioas para iniciar o seu teste.

### Avisos antes de começar ###

* Crie um repositório público no seu GitHub para realização do teste técnico
* Faça seus commits no seu repositório
* Envie o link do seu repositório para o e-mail ticredicom@gmail.com
*  Você poderá consultar um projeto qualquer na sua máquina, Stackoverflow ou o Google.

### Corpo do Email com o link do repositório do desafio ###

* Seu Nome Completo
* Link do Repositório
* Quanto tempo você tem de experiência em desenvolvimento utilizando PHP (Laravel) ?
* Quanto tempo você tem de experiência em desenvolvimento utilizando HTML + CSS + JS (Front-end) ?
* Quanto tempo você tem de experiência na utilização de versionamento de código/GIT (Commit, Push, Pull, Merge/Resolução de Conflitos) ?

### Sobre as tecnologias e abordagens no desenvolvimento deste desafio ###

* Recomendamos a utilização de versões mais recentes do PHP/Laravel
* Não é obrigatório a utilização de docker/containers
* Não é obrigatório a utilização de testes unitários
* Não recomendamos a realização de um commit único com todo o código do desafio
* Caso não seja possível finalizar o desafio todo não se preocupe, iremos avaliar o que foi desenvolvido/entregue.
* A interpretação deste desafio também faz parte da avaliação.
* Recomendamos a utilização de tratamento de exceção nas requisições do Backend
* Recomendamos avaliar a utilização de padrões (PSRs, design patterns, SOLID)

### Para o dia da entrevista técnica ###

Iremos realizar um Code Review como se você já fosse da nossa equipe, onde você poderá explicar o que você pensou, como arquitetou e como pode evoluir o projeto.

### Desafio ###

Este desafio será dividido em duas etapas:

##### "Backend" - API em Laravel #####

###### Requisitos: ######
* Essa API deverá ser protegida por um fluxo de autenticação
	- Ou seja, deverá ser obrigatório a informação de um token válido para algumas requisição nessa API
	- Este token precisa ter validade
* Essa API precisa realizar um "CRUD" em uma base de usuários
* Após um usuário realizar o "login" um token deverá ser atribuído a esse usuário e retornado na requisição
* Os usuários possuem apenas dois tipos: o Administrador e o Comum.
	- O Administrador pode realizar as operações de "CRUD" para qualquer usuário da base
		- É obrigatório informar o token, gerado no login, para qualquer operação
	- O Comum apenas poderá realizar as operações de "CRUD" para seu próprio usuário
		- Não é obrigatório informar o token para qualquer operação
* Após o usuário realizar o "logout" o seu respectivo token deverá ser excluído


##### "Frontend" - Um elemento de célula "parecido com Excel" #####

###### Requisitos: ######
* Para esta etapa do desafio, será necessário criar um elemento de uma tela no projeto em que seja possível realizar operações de soma, subtração, múltiplicação e divisão.
	- Segue exemplo:
    	- Ao digitar em uma "célula" o seguintes conjuntos de símbolos derá retornar o seguinte:
		> =2+2 => 4
		> 
		> =2-2 => 0
		> 
		> =2*2 => 4
		> 
		> =2/2 => 1
* Não é obrigatório criar um layout de autenticação para acessar essa "célula" e realizar as operações
* A etapa do "backend" e "frontend" não irão "conversar" em si para este desafio.
* Caso algo seja digitado incorretamente o sistema precisa notificar o usuário de alguma forma do erro ocorrido.
* Caso alguma operação der errado, por exemplo, divisão por 0, deverá ser retornado o valor "0".
* Após digitar o cálculo o sistema deverá substituir o texto digitado pelo resultado do cálculo formatado com milhar e decimal.